import Card from './Card'

export default function CardList({ robots }) {
  return (
    <ul >
      { robots.map(robot => <Card key={robot.id} { ...robot } />) }
    </ul>
  )
}